package ru.ifmo.rain.gilevich.crawler;

public interface ThreadPool extends AutoCloseable {

    void addTask(Runnable task);

    void close();
}
